#Bedtime

It's time to go to bed. Nouri can you get ready for bed? no, you need some instructions???? (for pretend)


def get_ready_for_bed {
1. Go upstairs
2. Go to bedroom
3. Open dresser
4. Get PJs out
5. Take off daytime clothes
6. Put on PJs
7. Close dresser door
8. Walk to bathroom (if hall light is off, turn it on)
9. Get out toothbrush and paste
10. Fill cup with water
11. Put paste on toothbrush
12. Dip toothbrush head in water of the cup
13. put toothbrush in mouth
14. brush (back and forth across top and bottom teeth for 2-3 minutes)
15. put water in mouth
16. gargle and spit into sink 2-3 times to get the toothpaste out
17. drink the remaining water (if thirsty)
18. clean toothbrush
19. put away toothbrush, paste, and cup
20. go potty (if necessary) 
21. walk to bedroom
22. straighten bed covers
23. take off hearing aids and glasses
24. get into bed
25. fall asleep (sweet dreams!!!)
}

get_ready_for_bed(nouri)

Alright Elka, your turn. Ready to go to bed?

get_ready_for_bed(Elka) (except the hearing aids and glasses line 29)

Alright Spencer, your turn. Ready to go to bed?

get_ready_for_bed(spencer) (except the hearing aids and glasses line 29)

Alright Shashana, your turn. Ready to go to bed?

get_ready_for_bed(shashi) (except the hearing aids and glasses line 29  )
