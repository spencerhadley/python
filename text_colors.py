import time

# ANSI escape codes for colors
RESET = "\033[0m"
RED = "\033[1;31m"
ORANGE = "\033[38;2;255;165;0m"
YELLOW = "\033[1;33m"
GREEN = "\033[1;36m"
BLUE = "\033[1;34m"
PURPLE = "\033[1;35m"
WHITE = "\033[37m"

# Construct the prompt with different colors
prompt = f"{RED}Red {ORANGE}Orange {YELLOW}Yellow {GREEN}Green {BLUE}Blue {PURPLE}Purple  {WHITE}White{RESET}"

