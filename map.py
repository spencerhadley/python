# Create the 2D list

map_array = []
for i in range(0, 21):
    rowi = []
    for j in range(0, 21):
        row.append('a lonely empty space')
    map_array.append(row)

# Fill the map_array with data
for i in range(21):
    map_array[0][i] = "The Kloth Mountain Range"
    map_array[i][0] = "The Ancient Glacier"
    map_array[i][20] = "The Cliff to the Void"
    map_array[20][i] = "The Scarcity Sea"


# Set specific locations
map_array[1][19] = "The Blacksmith's Cave"
map_array[1][18] = "Entrance to The Blacksmith's Cave"
map_array[1][17] = "a bramble with sounds behind it"
map_array[2][18] = "bushes with a banging sound nearby"
map_array[2][19] = "bushes with a banging sound nearby"
map_array[4][10] = "church alter"
map_array[5][10] = "center of church"
map_array[5][11] = "right pew"
map_array[5][9] = "left pew"
map_array[6][10] = "entrance to church"
map_array[7][10] = "church steps"
map_array[8][10] = "village in front of church"


# Function to print the map (for debugging)
def print_map(arr):
    for row in arr:
        print(row)

#Test the map
print_map(map_array)
