class MyMetaclass(type):
    def __new__(cls, name, bases, attrs):
        # Modify the class creation process here
        attrs['custom_attribute'] = 'This was added by the metaclass'
        return super().__new__(cls, name, bases, attrs)

class MyClass(metaclass=MyMetaclass):
    pass

print(MyClass.custom_attribute)  # Output: This was added by the metaclass
