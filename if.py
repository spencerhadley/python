import time
import sys

hello = input("Hello! hows your day?  ")

time.sleep(0.8)

if hello == "good":
    print("great!")

elif hello == "Good":
    print("great!")

elif hello == "great":
    print("great!")

elif hello == "Great":
    print("great!")

elif hello == "bad":
    print("I'm sorry to hear that.")

elif hello == "Bad":
    print("I'm sorry to hear that.")

else:
    print("Oops! Try typing 'bad' or 'good'")
    sys.exit()

time.sleep(0.8)

if hello == "bad":
    print("Well since you said your having a " + hello + " day let's try to cheer you up!")

elif hello == "Bad":
    print("Well since you said your having a " + hello + " day let's try to cheer you up!")
elif hello == "good":
    print("Well since you said your having a " + hello + " day let's try to cheer you up!")
else:
    print("Oops! Try typing 'bad' or 'good'")
    sys.exit()

time.sleep(0.8)

jokes = input("Ok! Why can't a nose be 12 inches long?  ")

if jokes == "why?":
    print("Because then it would be a foot!")

# time.sleep(0.6)

elif jokes == "why?":
    print("Ba Dump Ba Dump!...")

# time.sleep(3)

elif jokes == "Why?":
    print("Because then it would be a foot!")

else:
    print("Oops! Try typing 'why?'")

sys.exit()

