import tkinter as tk

def rgb_to_hex(rgb):
    """Convert RGB tuple to hexadecimal color code"""
    return '#{:02x}{:02x}{:02x}'.format(rgb[0], rgb[1], rgb[2])

def update_color():
    """Update the background color based on slider values"""
    r = red_slider.get()
    g = green_slider.get()
    b = blue_slider.get()
    color = rgb_to_hex((r, g, b))
=5
