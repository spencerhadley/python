# import time #This line downloads a library and so we can use the time.sleep() command
# # from .time import hi
# print("  File '/home/nouri/python/Inputs.py', line 26")
# print("          ^")
# print("SyntaxError: invalid syntax")
# fake_derectory = input("nouri@pop-os:~/python$ ")
# time.sleep(5)
# print("Just kidding!")
# time.sleep(0.9)
# name = input("What's your name? ") # 'name' creates a varible with input and ("What's your name?  ") inside, whatever you type in the Terminal goes into 'name'
# print(" processing please wait...")
# time.sleep(2) # this line allows the text to have a moment pause before continuing. The (1) at the end represents 1 second. Time.sleep is a function from time in line 2 it inports the time library into this process 'the time in time.sleep says "Hey computer! go and open the time library" when the library is open then the computer takes the sleep funcion out.
# print(" Generating responce...")
# time.sleep(2)
# print("Hello, " + name + "!") # Line 7 starts with a print function then says ("Hello, " + name + "!") the print funtion just basicly prints something whatever you tell it to print (like a printer) then after there is '("Hello, "' this part just tells the computer to print out "Hello, ". The next part '+ name +' this part this just says "add name" and it means the function from line 5 so whatever you typed in there it would print out too so it would say something like-"Hello, ___" then last but not least, the next part just adds a !. so it now says "Hello, ___!"
# time.sleep(0.8)
# color = input("What is your favorite color " + name + "?  ")
#
# print(" processer is activated...")
# time.sleep(2)
#
# print (" getting ready...")
# time.sleep(0.7)
# time.sleep(1)
#
#
# time.sleep(1)
# print("I like " + color + " too!")
# time.sleep(2)
# animal = input("What is you favorite animal " + name + "?  ")
# time.sleep(0.7)
#
# print(animal + " that's a cool animal!")
# time.sleep(2)
#
# goodbye = "Well " + name + " thanks for talking with me!"
# print(goodbye)
# time.sleep(1)

import time

# ANSI escape codes for colors
RESET = "\033[0m"
BRIGHT_GREEN = "\033[1;32m"
LIGHT_BLUE = "\033[1;34m"
WHITE = "\033[37m"

print("  File '/home/nouri/python/Inputs.py', line 63")
print(" processing please wait...")
print("                        ^")
print("SyntaxError: invalid syntax")

# Construct the prompt with different colors
prompt = f"{BRIGHT_GREEN}nouri@pop-os{WHITE}:{LIGHT_BLUE}~/python{RESET}$ "
fake_directory = input(prompt)

time.sleep(0.3)
print("Just kidding!")
time.sleep(0.9)

# Rest of your code remains the same
name = input("What's your name? ")
print(" processing please wait...")
time.sleep(2)
print(" Generating responce...")
time.sleep(2)
print("Hello, " + name + "!")

time.sleep(0.8)
color = input("What is your favorite color " + name + "?  ")

print(" processer is activated...")
time.sleep(2)

print (" getting ready...")
time.sleep(0.7)
time.sleep(1)


time.sleep(1)
print("I like " + color + " too!")
time.sleep(2)
animal = input("What is you favorite animal " + name + "?  ")
time.sleep(0.7)

print(animal + " that's a cool animal!")
time.sleep(2)

goodbye = "Well " + name + " thanks for talking with me!"
print(goodbye)
time.sleep(1)
