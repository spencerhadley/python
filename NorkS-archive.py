#python3 NorkS.py
print("You're in a clearing. There is a tower in front of you.")
print("What do you want to do?")

while True:
    action = input("> ").lower()
    if action == "whats that!?":
        print("There's a sword in some stone. Wait! That sounds familiar!!!")
    elif action == "look around":
        print("North: Tower")
        print("East: Forest")
        print("West: River")
        print("South: Hole")
    elif action == "look for flowers":
        print("There is a beautiful field of flowers. It smells so good! Look at all the colors. Inspiring!!")
    elif action in ["north", "south", "east", "west"]:
        if action == "north":
            print("You approach the tower...")
        elif action == "east":
            print("You head towards the forest...")
        elif action == "west":
            print("You walk towards the river...")
        elif action == "south":
            while True:
                hole = input("You trudge towards the hole... You've made it to the Hole. It looks to be about six feet deep. There's an old rope beside it. Do you want to climb down the hole or go back > ").lower()
                if hole == "climb down":
                    print("You climb down into the hole...")
                    print("It is dark in here, be careful. Your foot touches an unlit lamp. What do you want to do?")
                    while True:
                        hole_action = input("> ").lower()
                        if hole_action == "pick up the lamp":
                            print("You now have the lamp.")
                            break
                        else:
                            print("Try typing 'pick up the lamp'")
                    break
                elif hole == "go back":
                    print("You return to the clearing.")
                    break
                else:
                    print("Please type 'climb down' or 'go back'.")
    elif action == "exit":
        confirm = input("Do you want to stop? Yes or No: ").lower()
        if confirm == "yes":
            print("Thanks for playing NorkS! Goodbye!")
            break
        elif confirm == "no":
            print("Okay, let's continue the adventure!")
        else:
            print("I didn't understand that. Let's continue the adventure!")
    else:
        print("Try typing a direction.")