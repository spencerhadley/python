def growing_bar(duration=3):
    start_time = time.time()
    while time.time() - start_time < duration:
        for i in range(10):
            print(f"\r[{'#' * i}{' ' * (9-i)}]", end="", flush=True)
            time.sleep(0.1)

def bouncing_ball(duration=3):
    animation = "⠁⠂⠄⠂"
    idx = 0
    start_time = time.time()
    while time.time() - start_time < duration:
        print(f"\r{animation[idx % len(animation)]}", end="", flush=True)
        idx += 1
        time.sleep(0.2)
