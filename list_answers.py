#Spencer's attempt to join the functions
#a, b = [[165,11],[276,12],[1164,12],[3792,12],[316,4],[220,10]]

#question_number = index[x+1]

#for x in (a, b):
#    result = a // b
#    return(result)

#print("Question f{question_number} answer is", result)


a = [[165,11],[276,12],[1164,12],[3792,12],[316,4],[220,10]]

results = []
bigger = []
smaller = []

for pair in a:
    result = pair[0] // pair[1]
    bigger_number = pair[0]
    smaller_number = pair[1]
    results.append(result)
    bigger.append(bigger_number)
    smaller.append(smaller_number)

for i, result in enumerate(results, start=1):
    print(f"{bigger[i-1]} divided by {smaller[i-1]} is {result}")


#165 / 11
#276 / 12
#1164 / 12
#3792 / 12
#316 / 4
#220 / 10
