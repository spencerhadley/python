#Calculator

print("What kind of math do you want to do?\n +) addition\n -) subtraction\n *) multiplication\n /) division\n \n Type one Enter + - * or /")

math_type = input()

def multiplication(a,b):
	answer = a * b
	return answer

def division(a,b):
	answer = a // b
	return answer

def subtraction(a,b):
	answer = a - b
	return answer

def addition(a,b):
	answer = a + b
	return answer


if math_type in ["+", "-", "*", "//"]:
	first_number = int(input("Enter the first number: "))
	second_number = int(input("Enter the second number: "))
	if math_type == "+":
		print(str(first_number) + " + " + str(second_number) + " = " + str(addition(first_number,second_number)))
	
	elif math_type == "-":
		print(str(first_number) + " - " + str(second_number) + " = " + str(subtraction(first_number,second_number)))

	elif math_type == "*":
		print(str(first_number) + " * " + str(second_number) + " = " + str(multiplication(first_number,second_number)))
	
	elif math_type == "//":
		print(str(first_number) + " // " + str(second_number) + " = " + str(division(first_number,second_number)))
	
	else:
		print("try again?")

else:
	print("Invalid operation. Please try again.")

#if math_type in ["+", "-", "*", "/"]:
#    first_number = int(input("Enter the first number: "))
#    second_number = int(input("Enter the second number: "))
#    if math_type == "+":
#        print(f"{first_number} + {second_number} = {addition(first_number, second_number)}")
#    elif math_type == "-":
#        print(f"{first_number} - {second_number} = {subtraction(first_number, second_number)}")
#    elif math_type == "*": 
#        print(f"{first_number} * {second_number} = {multiplication(first_number, second_number)}")
#    elif math_type == "/":
#        if second_number != 0:
#            print(f"{first_number} / {second_number} = {division(first_number, second_number)}")
#        else:
#            print("Error: Division by zero is not allowed.")
#else:
#    print("Invalid operation. Please try again.")
