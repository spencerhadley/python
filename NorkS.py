from map import map_array

# Initialize the 2D list with the imported map_array
list_2d = [row[:] for row in map_array]

# Function to set the position of the player
def set_player_position(row, col, previous_position=None):
    if previous_position:
        prev_row, prev_col = previous_position
        list_2d[prev_row][prev_col] = map_array[prev_row][prev_col]

    list_2d[row][col] = "Player"
    return (row, col)

# Function to get a string from a specific position
def get_string(row, col):
    return list_2d[row][col]

# Function to print the map
def print_map():
    for row in list_2d:
        print(row)

# Example usage
player_position = set_player_position(10, 10)
print("Initial map:")
print_map()

print(f"\nThe player is at position: row {player_position[0]}, column {player_position[1]}")
print(f"Location at (0,0): {get_string(0,0)}")

# Move the player
player_position = set_player_position(11, 11, player_position)
print(f"\nAfter move the player is at position: row {player_position[0]}, column {player_position[1]}")
# print("\nMap after moving the player:")
# print_map()
