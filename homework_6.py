import time
import homework_5
import sys
import readline
import os
import atexit
import random
import text_colors

terminal_color = text_colors.WHITE
histfile = os.path.join(os.path.expanduser("~"), ".python_history")

try:
    readline.read_history_file(histfile)
    readline.set_history_length(1000)
except FileNotFoundError:
    pass

atexit.register(readline.write_history_file, histfile)

def colored_string(x):
    return f"\r{terminal_color}{x}"


def loading_animation(duration=3):
    hey = 0
    start_time = time.time()
    while time.time() - start_time < duration:
        print(f"\rLOADING", end="", flush=True)
        hey += 1
        print("...")

def spinning_line(duration=3):
    animation = "|/-\\| ITISDONE"
    idx = 0
    start_time = time.time()
    while time.time() - start_time < duration:
        print(f"\r{animation[idx % len(animation)]}", end="", flush=True)
        idx += 1
        time.sleep(0.2)

def bouncing_ball(duration=0.7):
    animation = "⠁⠂⠄⠂"
    idx = 0
    start_time = time.time()
    while time.time() - start_time < duration:
        print(f"\r{animation[idx % len(animation)]}", end="", flush=True)
        idx += 1
        time.sleep(0.1)

# def print(x):
#     print(f"\r{terminal_color}{x}")

# def loading_animation(duration=3):
#     idx = 0
#     start_time = time.time()    #start_time = 3:21:00 pm
#     while time.time() - start_time < duration:   #3:21:01 - 3:21:00 < 3
#         print(f"\rLoading{('.' * (idx % 3 + 1)):3}", end="", flush=True)
#         idx += 1
#         time.sleep(0.3)
#
#     print("\rLoaded!    ")

while True:
    # Glitch effect for the greeting
    greeting_text = (colored_string("Hello! Would you like to either add words, or do the calculator? (Tip: just type commands!)"))
    glitch_chars = "!@#$%^&*()_+-=[]|:;<>,.?/~`"
    text_length = len(greeting_text)

    for _ in range(10):  # Show 10 glitched versions
        glitched = ''.join(random.choice(glitch_chars) if random.random() < 0.3 else c for c in greeting_text)
        glitched = glitched[:-5]
        print(f"\r{glitched}", end="", flush=True)
        time.sleep(0.1)

    print(f"\r{greeting_text}", end="")  # Print the final, correct version without newline
    greeting = input("  ")  # Two spaces for alignment, input on the same line


    if greeting.lower() in ['exit', 'e']:
        break

    elif greeting.lower() in ('commands'):
        print(colored_string("These are all the commands that are useable:"))
        bouncing_ball()
        print(colored_string("exit"))
        bouncing_ball()
        print(colored_string("calculator"))
        bouncing_ball()
        print(colored_string("add words"))
        bouncing_ball()
        print(colored_string("algabra"))
        bouncing_ball()
        print(colored_string("settings"))
        time.sleep(0.7)

    elif greeting.lower() in ['calculator', "c", 'calc']:
        numbers = input(colored_string("Give me two numbers! ")).split()
        if len(numbers) == 1 and numbers[0].lower() in ['exit', 'e']:
            break
        elif len(numbers) != 2:
            print(colored_string("Error: Please type two numbers divided by a space."))
            print("                                                  ")
            continue
        try:
            num1 = int(numbers[0])
            num2 = int(numbers[1])
        except ValueError:
            print(colored_string("Error: Invalid input, please enter valid integers."))
            continue

        time.sleep(1)
        arithmetic = input(colored_string("Great! now what arithmetic do you want to use? "))

        if arithmetic.lower() in ['exit', 'e']:
            break
        elif arithmetic == '+':
            result = homework_5.addition(num1, num2)
        elif arithmetic == '-':
            result = homework_5.subtraction(num1, num2)
        elif arithmetic == '*':
            result = homework_5.multiplication(num1, num2)
        elif arithmetic == '/':
            result = homework_5.division(num1, num2)
        else:
            print(colored_string("Error: invalid please type (+,-,*,/)"))
            continue

        print(colored_string("The answer is "))
        spinning_line()
        print(colored_string(result))

        continue

    elif greeting.lower() in ["add words", "aw", "addw", "add w", "a w"]:
        letters = input("Give me some letters divided by a space. ").split()
        if len(letters) < 2:
            print("Error: Please type at least two letters divided by a space.")
            continue
        elif any(not letter.isalpha() for letter in letters):
            print("Error: Please enter only letters.")
            continue
        elif letters[0].lower() == 'exit':
            break
        else:
            adding = ''.join(letters)

            print("The answer is")
            loading_animation()
            print(adding)

    elif greeting.lower() in ["algabra", "a"]:
        algabra_greeting = input("")

    elif greeting.lower() in ["settings", "s"]:
        # print("What would you like do do?")
        bouncing_ball()
        print(colored_string("Text color"))
        bouncing_ball()
        print(colored_string("Typing speed"))

        settings_choice = input("Do you want to change color or speed?  ")
        if settings_choice.lower() in ["color", "c"]:
            tc = input("What color do you want?  ")
            tc = tc.lower()
            match tc:
                case "red"|"r":
                    terminal_color = text_colors.RED
                case "orange"|"o":
                    terminal_color = text_colors.ORANGE
                case "yellow":
                    terminal_color = text_colors.YELLOW
                case "green":
                    terminal_color = text_colors.GREEN
                case "blue":
                    terminal_color = text_colors.BLUE
                case "purple":
                    terminal_color = text_colors.PURPLE
                case "white":
                    terminal_color = text_colors.WHITE

        else:
            settings_choice.lower() in ['exit', 'e']
            break



                # case "reset":
                #     terminal_color = text_colors.RESET

    elif greeting.lower() in ["hello! do you know who i am?", "hdykwia"]:
        print(colored_string("No..."))
        time.sleep(0.6)
        print(colored_string("But i know"))
        time.sleep(0.9)
        print(colored_string("not that much!"))
        time.sleep(0.3)
        print(colored_string(">:)"))

    elif greeting.lower() in ["hmm"]:
        confused = input("Are you confused?  ")
        if confused.lower() in ["Yes"]:
            print(colored_string("Try typing c/calculator or add words/aw"))

        time.sleep(1)
        continue

        # elif :
        #     answer = "Error: invalid please type +"

    else:
        print("Oops! Try typing calculator or add words.")
