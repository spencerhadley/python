import random

#Good bacteria
bacteria_1 = ["Bifidobacteria", "These make up a large portion of the beneficial bacteria in our gut microbiome. They help with digestion and nutrient absorption"]
bacteria_2 = ["Lactobacillus", "Found in the digestive, urinary, and genital systems, these bacteria aid in digestion and help prevent harmful bacteria from growing"]
bacteria_3 = ["treptococcus thermophilus", "This bacterium produces lactase, which helps digest dairy products"]
bacteria_4 = ["Akkermansia", "A beneficial gut bacterium that helps maintain a healthy intestinal lining"]
bacteria_5 = ["Rhizobacteria", "These soil bacteria provide ammonia to plants, aiding in their growth"]
good_bacteria = [bacteria_1, bacteria_2, bacteria_3, bacteria_4, bacteria_5]
random_good = random.choice(good_bacteria)



#Bad bacteria
bacteria_6 = ["Streptococcus pneumoniae", "Can cause pneumonia", "human"]
bacteria_7 = ["Haemophilus influenzae", "Can lead to meningitis", "animal"]
bacteria_8 = ["Escherichia coli (E. coli)", "Some strains can cause food poisoning"]
bacteria_9 = ["Salmonella", "Another cause of food poisoning"]
bacteria_10 = ["Staphylococcus aureus", "Can cause various infections, from minor skin issues to more serious conditions like pneumonia"]

bad_bacteria = [bacteria_6, bacteria_7, bacteria_8, bacteria_9, bacteria_10]
random_bad = random.choice(bad_bacteria)

prompt = "Do you want to know about good bacteria or bad bacteria? (Answer good bacteria or bad bacteria)"

print (prompt)
answer = input()

if answer == "good bacteria":
	print ("Here's the name: " + random_good[0] + ". Here's some info: " + random_good[1])
elif answer == "bad bacteria":
	print ("Here's the name: " + random_bad[0] + ". Here's some info: " + random_bad[1])
else:
	print ("please answser with 'good bacteria' or 'bad bacteria'")

# homework
# if answer is good and "Rhizobacteria" then pick a different bacteria
# add another parameter
# add another input from user
# good & human == good human bacterias
# bad & human == bad human bacterias
